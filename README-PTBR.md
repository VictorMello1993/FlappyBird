<h2>:earth_americas: Visão geral</h2>
<p>Projeto de construção do jogo Flappy Bird rodando na versão desktop, promovido pela <a href="https://github.com/cod3rcursos">Cod3r Cursos</a>, Curso Web Moderno 2020</p>

![Hnet-image](https://user-images.githubusercontent.com/35710766/93035639-eed57700-f613-11ea-9cd9-cb98dc2b5a41.gif)

<h2>:wrench: Recursos </h2>
<ul>
  <li>HTML</li>
  <li>CSS</li>
  <li>JavaScript</li>
</ul>

<h2>:triangular_flag_on_post: Começando</h2>
A aplicação é bem simples, pois roda 100% no navegador, sem back-end. Para executá-la, basta seguir o seguinte passo a passo:

1 - Certificar-se de que tenha instalado a extensão <strong>Live Server</strong> no VS Code. Em caso negativo, vá em gerenciador de pacotes do VS Code para pesquisar por ela e instalar, conforme a imagem abaixo:
![Screenshot_1](https://user-images.githubusercontent.com/35710766/93033635-779ce480-f60d-11ea-8014-9c6f0d7bbcd4.png)

2 - Em ```flappy.html``` Pressionar F1 para executar o comando <strong>Live Server: Open with Live Server</strong> que será aberta uma janela do navegador automaticamente. O interessante desta extensão é que uma vez que você aplica o comando, após qualquer alteração no código, não é necessário aplicá-lo de novo. Basta salvar o arquivo com CTRL+S que o navegador automaticamente já renderiza.
![Screenshot_2](https://user-images.githubusercontent.com/35710766/93034061-30175800-f60f-11ea-8ef3-f920356778be.png)

3 - Pronto, a aplicação já está em execução! Divirta-se! =)
![Screenshot_3](https://user-images.githubusercontent.com/35710766/93034125-65bc4100-f60f-11ea-998b-f4949209cc91.png)
    
  
  
